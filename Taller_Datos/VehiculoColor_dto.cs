﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taller_Datos
{
    public class VehiculoColor_dto
    {
        public int id_tipo_vehiculo_color { get; set; }

        public string descr_tipo_color { get; set; }
    }
}
