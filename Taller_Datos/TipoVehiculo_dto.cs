﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taller_Datos
{
    public class TipoVehiculo_dto
    {
        public int id_tipo { get; set; }
        public string descrip_tipo { get; set; }
    }
}
