﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taller_Datos
{
    public class Detalle_Servicio_dto
    {
        public int IdServicio { get; set; }

        public string Servicio { get; set; }

        public int ValorServicio { get; set; }
    }
}
