﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using Taller_Datos;
using Taller_Negocio;

namespace Taller_Escritorio_wpf
{
    /// <summary>
    /// Lógica de interacción para ModuloVehiculo.xaml
    /// </summary>
    public partial class ModuloVehiculo : Window
    {
        public ModuloVehiculo()
        {
            InitializeComponent();
            CargarComboBoxTipoVehiculo();
        }

        public  void CargarComboBoxTipoVehiculo()
        {
            //////////////LISTAR TIPO VEHICULO//////////////////
            TipoVehiculo_Negocio tipovehiculoN = new TipoVehiculo_Negocio();
            cmb_id_tipo_vehiculo.ItemsSource = tipovehiculoN.ListarVehiculo();
            cmb_id_tipo_vehiculo.DisplayMemberPath = "descrip_tipo";
            cmb_id_tipo_vehiculo.SelectedValuePath = "id_tipo";

            //////////////LISTAR FAMILIA/////////////////
            cmb_Color.ItemsSource = tipovehiculoN.ListarVehiculoColor();
            cmb_Color.DisplayMemberPath = "descr_tipo_color";
            cmb_Color.SelectedValuePath = "id_tipo_color";
        }

        private void BotonCerrar_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        //private void limpiarVehiculo()
        //{
        //    txt_Id_Vehiculo.Text == "";
        //    txt_Patente.Text == "";
        //    txt_Marca.Text == "";
        //    txt_Modelo.Text == "";
        //    txt_Color.Text == "";
        //    txt_Id_Cliente.Text == "";
        //    cmb_id_tipo_vehiculo.SelectedValue = null;
        //}

        private void Btn_Confirmar_Vehiculo_Click(object sender, RoutedEventArgs e)
        {
            bool resp = false;
            Vehiculo_Negocio vehN = new Vehiculo_Negocio();

            if (txt_Patente.Text == "" && txt_Marca.Text == "" && txt_Modelo.Text == "" && cmb_Color.Text == "" && txt_Id_Cliente.Text == "" && cmb_id_tipo_vehiculo.Text == "")
            {
                if(txt_Patente.Text == "" && txt_Marca.Text == "" && txt_Modelo.Text == "" && cmb_Color.Text == "" && txt_Id_Cliente.Text == "" && cmb_id_tipo_vehiculo.Text == "")
                {
                    MessageBox.Show("Debe completar todos los campos");
                }
                else
                {
                    if(txt_Patente.Text == "" && !resp)
                    {
                        MessageBox.Show("Debe Ingresar Patente");
                    }
                    if (txt_Marca.Text == "" && !resp)
                    {
                        MessageBox.Show("Debe Ingresar Marca");
                    }
                    if (txt_Modelo.Text == "" && !resp)
                    {
                        MessageBox.Show("Debe Ingresar Modelo");
                    }
                    if (cmb_Color.Text == "" && !resp)
                    {
                        MessageBox.Show("Debe Ingresar Color");
                    }
                    if (cmb_id_tipo_vehiculo.Text == "" && !resp)
                    {
                        MessageBox.Show("Debe Ingresar Tipo Vehículo");
                    }
                }
            }
            else
            {
                bool resultado = false;
                try
                {
                    resultado = vehN.crearVehiculo(txt_Id_Vehiculo.Text, txt_Patente.Text, txt_Marca.Text, txt_Modelo.Text, cmb_Color.Text,
                                                   txt_Id_Cliente.Text, cmb_id_tipo_vehiculo.SelectedValue.ToString());

                    if (resultado)
                    {
                        if(txt_Id_Cliente.Text != "")
                        {
                            MessageBox.Show("Vehículo modificado");
                        }
                        else
                        {
                            MessageBox.Show("Vehículo Registrado");
                           
                        }
                    }
                    else
                    {
                        MessageBox.Show("Vehículo no pudo ser creado");
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Vehículo no pudo ser creado");
                }
            }
        }

        private void Btn_MenuP_Click(object sender, RoutedEventArgs e)
        {

        }

        private void txt_Color_TextChanged(object sender, TextChangedEventArgs e)
        {

        }
    }
}
