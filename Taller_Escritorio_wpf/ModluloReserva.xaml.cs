﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using Taller_Datos;
using Taller_Negocio;
using System.Text.RegularExpressions;
using System.Diagnostics.Contracts;
using System.Xml.Serialization.Configuration;
using System.Windows.Media;

namespace Taller_Escritorio_wpf
{
    /// <summary>
    /// Lógica de interacción para ModluloReserva.xaml
    /// </summary>
    public partial class ModluloReserva : Window
    {
        public ModluloReserva()
        {
            InitializeComponent();
            CargarServicio();
        }

        public void CargarServicio()
        {
            //////////////LISTAR SERVICIO//////////////////
            Compartido_Negocio ServicioN = new Compartido_Negocio();
            cmb_Estado.ItemsSource = ServicioN.ListarEstado();
            cmb_Estado.DisplayMemberPath = "id_descr_estado_r";
            cmb_Estado.SelectedValuePath = "id_estado_r";
        }

        private void BotonCerrar_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        public void LimpiarReserva()
        {
            txt_Id_Reserva.Text = "";
            txt_Id_Rut.Text = "";
            txt_Id_P_N_Cliente.Text = "";
            txt_Id_P_A_Cliente.Text = "";
            cmb_Servicio.Text = "";
            cmb_Empleado.Text = "";
            cmb_Hora.Text = "";
            cmb_Estado.Text = "";
            fecha_Reserva.Text = "";
        }

        private void Btn_Buscar_reserva_Click(object sender, RoutedEventArgs e)
        {
            Reserva_Negocio reserN = new Reserva_Negocio();
            DataTable resp = new DataTable();
            Compartido_Negocio comp = new Compartido_Negocio();
            try
            {
                resp = reserN.ListarReservaPorIDCliente(txt_Id_Reserva.Text);

                if (resp.Rows.Count > 0)
                {
                    foreach (DataRow item in resp.Rows)
                    {
                        txt_Id_Reserva.Text = item["ID_RESERVA"].ToString();
                        txt_Id_Rut.Text = item["RUT_CLIENTE"].ToString();
                        txt_Id_P_N_Cliente.Text = item["P_NOMBRE_CLIENTE"].ToString();
                        txt_Id_P_A_Cliente.Text = item["P_APELLIDO_CLIENTE"].ToString();
                        cmb_Servicio.Text = item["DESC_SERVICIO"].ToString();
                        cmb_Empleado.Text= item["P_NOMBRE_EMPLEADO"].ToString();
                        cmb_Hora.Text = item["HORA_RESERVA"].ToString();
                        cmb_Estado.Text = item["ID_DESCR_ESTADO_R"].ToString();
                        fecha_Reserva.Text = item["FECHA_RESERVA"].ToString();
                    }
                }
                else
                {
                    MessageBox.Show("ID Reserva inválido");

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Busqueda no arrojó resultados");
                throw ex;
            }
        }

        private void Btn_Buscar_rut_Click(object sender, RoutedEventArgs e)
        {
            Reserva_Negocio reserN = new Reserva_Negocio();
            DataTable resp = new DataTable();
            Compartido_Negocio comp = new Compartido_Negocio();
            try
            {
                resp = reserN.ListarReservaPorRutCliente(txt_Id_Rut.Text);

                if (resp.Rows.Count > 0)
                {
                    foreach (DataRow item in resp.Rows)
                    {
                        txt_Id_Reserva.Text = item["ID_RESERVA"].ToString();
                        txt_Id_Rut.Text = item["RUT_CLIENTE"].ToString();
                        txt_Id_P_N_Cliente.Text = item["P_NOMBRE_CLIENTE"].ToString();
                        txt_Id_P_A_Cliente.Text = item["P_APELLIDO_CLIENTE"].ToString();
                        cmb_Servicio.Text = item["DESC_SERVICIO"].ToString();
                        cmb_Empleado.Text = item["P_NOMBRE_EMPLEADO"].ToString();
                        cmb_Hora.Text = item["HORA_RESERVA"].ToString();
                        cmb_Estado.Text = item["ID_DESCR_ESTADO_R"].ToString();
                        fecha_Reserva.Text = item["FECHA_RESERVA"].ToString();
                    }
                }
                else
                {
                    MessageBox.Show("RUT Reserva inválido");

                }


            }
            catch (Exception ex)
            {
                MessageBox.Show("Busqueda no arrojó resultados");
                throw ex;
            }
        }



        private void Btn_Actualizar_Estado_Reserva_Click(object sender, RoutedEventArgs e)
        {
            Reserva_Negocio ReservaN = new Reserva_Negocio();
            bool resultado = false;
            try
            {
                resultado = ReservaN.actualizarEstadoReserva(txt_Id_Reserva.Text,cmb_Estado.SelectedValue.ToString());
            }
            catch(Exception)
            {
                throw;
            }
        }

        private void btn_MenuP_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
            MenuPrincipal ventana = new MenuPrincipal();
            ventana.ShowDialog();
        }

        private void Btn_Limpar_Reserva_Click(object sender, RoutedEventArgs e)
        {
            LimpiarReserva();
        }

        private void Btn_pedidos_Click(object sender, RoutedEventArgs e)
        {
            //this.Hide();
            //ModuloPedido ventana = new ModuloPedido();
            //ventana.ShowDialog();
        }

        private void Btn_recep_p_Click(object sender, RoutedEventArgs e)
        {
            //this.Hide();
            //RecepcionPedido ventana = new RecepcionPedido();
            //ventana.ShowDialog();
        }
    }
}
