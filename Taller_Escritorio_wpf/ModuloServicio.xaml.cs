﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using Taller_Datos;
using Taller_Negocio;
using System.Text.RegularExpressions;
using System.Diagnostics.Contracts;
using System.Xml.Serialization.Configuration;
using System.Windows.Media;
using System.Data.SqlTypes;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace Taller_Escritorio_wpf
{
    /// <summary>
    /// Lógica de interacción para ModuloServicio.xaml
    /// </summary>
    public partial class ModuloServicio : Window
    {
        public ModuloServicio()
        {
            InitializeComponent();
        }


        private void BotonCerrar_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Btn_Buscar_Reserva_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Btn_Agregar_Reserva_Click(object sender, RoutedEventArgs e)
        {
            Servicio_Negocio servicioNeg = new Servicio_Negocio();
            bool resultado = false;
            try
            {
                resultado = servicioNeg.crearServicio(txt_Id_Servicio.Text, txt_Servicio.Text, txt_Precio_Servicio.Text);
                if (resultado)
                {
                    MessageBox.Show("Servicio creado");

                }
                else
                {
                    MessageBox.Show("Servicio no pudo ser creado");
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Servicio no pudo ser creado2");
                throw;

            }
        }

        public void limpiarServicio()
        {
            txt_Id_Servicio.Text = "";
            txt_Precio_Servicio.Text = "";
            txt_Servicio.Text = "";
        }

        private void Btn_Mostrar_S_Click(object sender, RoutedEventArgs e)
        {
            {
                InitializeComponent();
                List<Detalle_Servicio_dto> ListarServicio()
                {
                    OracleComand exec = new OracleComand();
                    List<Detalle_Servicio_dto> retorno = new List<Detalle_Servicio_dto>();
                    try
                    {
                        var Parameters = new Dictionary<string, string>();
                        DataTable dataTable = new DataTable();
                        exec.ExecStoredProcedure("SP_DDL_LISTAR_SERVICIO", dataTable, Parameters);
                        foreach (DataRow rows in dataTable.Rows)
                        {
                            Detalle_Servicio_dto entidad = new Detalle_Servicio_dto();
                            entidad.IdServicio = int.Parse(rows["id_servicio"].ToString());
                            entidad.Servicio = rows["desc_servicio"].ToString();
                            entidad.ValorServicio = int.Parse(rows["valor_servicio"].ToString());
                            retorno.Add(entidad);
                            entidad = null;
                        }

                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                    return retorno;
                }

                dgr_S_P.ItemsSource = ListarServicio();
            }
        }
    
    }
}
