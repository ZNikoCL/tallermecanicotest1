﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using Taller_Datos;
using Taller_Negocio;
using System.Text.RegularExpressions;
using System.Diagnostics.Contracts;
using System.Xml.Serialization.Configuration;
using System.Windows.Media;

namespace Taller_Escritorio_wpf
{
    /// <summary>
    /// Lógica de interacción para ModuloEmpresa.xaml
    /// </summary>
    public partial class ModuloEmpresa : Window
    {
        public ModuloEmpresa()
        {
            InitializeComponent();
        }
        private void BotonCerrar_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
        private void Border_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
        }

        private void Btn_Confirmar_Empresa_Click(object sender, RoutedEventArgs e)
        {
            bool resp = false;
            EmpresaCli_Negocio EmpresaCliN = new EmpresaCli_Negocio();
            if (txt_Giro_Empresa.Text == "" || txt_Razon_Social.Text == "" || txt_Rut_Empresa.Text == "" || txt_Dv_Empresa.Text == "" || txt_Direccion_Empresa.Text == "" || txt_Numeracion_Empresa.Text == "" || 
                txt_Fono_Empresa.Text== "") 
            {
                if (txt_Giro_Empresa.Text == "" && txt_Razon_Social.Text == "" && txt_Rut_Empresa.Text == "" && txt_Dv_Empresa.Text == "" && txt_Direccion_Empresa.Text == "" && txt_Numeracion_Empresa.Text == "" && 
                    txt_Fono_Empresa.Text == "")
                {
                    MessageBox.Show("Debe completar todos los campos");
                }
                else
                {
                    if (txt_Giro_Empresa.Text == "" && !resp)
                    {
                        MessageBox.Show("Debe Ingresar Giro");
                    }
                    if (txt_Razon_Social.Text == "" && !resp)
                    {
                        MessageBox.Show("Debe Ingresar Razón Social");
                    }
                    if (txt_Rut_Empresa.Text == "" && !resp)
                    {
                        MessageBox.Show("Debe Ingresar Rut de la Empresa");
                    }
                    if (txt_Dv_Empresa.Text == "" && !resp)
                    {
                        MessageBox.Show("Debe Ingresar número verificador de la empresa");
                    }
                    if (txt_Direccion_Empresa.Text == "" && !resp)
                    {
                        MessageBox.Show("Debe Ingresar Dirección");
                    }
                    if (txt_Numeracion_Empresa.Text == "" && !resp)
                    {
                        MessageBox.Show("Debe Ingresar Tipo Numeración");
                    }
                    if (txt_Fono_Empresa.Text == "" && !resp)
                    {
                        MessageBox.Show("Debe Ingresar Fono");
                    }
                }

            }
            else
            {
                bool resultado = false;
                try
                {
                    resultado = EmpresaCliN.crearEmpresa(txt_Id_Empresa.Text,txt_Giro_Empresa.Text, txt_Razon_Social.Text, txt_Rut_Empresa.Text, txt_Dv_Empresa.Text, txt_Direccion_Empresa.Text,
                                                    txt_Numeracion_Empresa.Text, txt_Fono_Empresa.Text, txt_Id_Cliente.Text);

                    if (resultado)
                    {
                        if (txt_Id_Cliente.Text != "")
                        {
                            MessageBox.Show("Empresa modificado");
                        }
                        else
                        {
                            MessageBox.Show("Empresa Registrado");
                            //limpiarVehiculo();
                        }
                    }
                    else
                    {
                        MessageBox.Show("Empresa no pudo ser creado");
                    }
                }
                catch (Exception)
                {
                    MessageBox.Show("Empresa no pudo ser creado");
                }
            }
        }
    }
    }

