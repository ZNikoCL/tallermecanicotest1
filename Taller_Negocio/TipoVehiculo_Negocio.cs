﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Taller_Datos;


namespace Taller_Negocio
{
    public class TipoVehiculo_Negocio
    {
        public List<TipoVehiculo_dto> ListarVehiculo()
        {
            OracleComand exec = new OracleComand();

            List<TipoVehiculo_dto> retorno = new List<TipoVehiculo_dto>();
            try
            {
                var Parameters = new Dictionary<string, string>();
                DataTable dataTable = new DataTable();
                exec.ExecStoredProcedure("SP_DDL_LISTAR_T_VEHI", dataTable, Parameters);
                foreach (DataRow rows in dataTable.Rows)
                {
                    TipoVehiculo_dto entidad = new TipoVehiculo_dto();
                    entidad.id_tipo = int.Parse(rows["id_tipo"].ToString());
                    entidad.descrip_tipo = rows["descrip_tipo"].ToString();
                    retorno.Add(entidad);
                    entidad = null;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return retorno;
        }


        public List<VehiculoColor_dto> ListarVehiculoColor()
        {
            OracleComand exec = new OracleComand();

            List<VehiculoColor_dto> retorno = new List<VehiculoColor_dto>();

            try
            {
                var Parameters = new Dictionary<string, string>();
                DataTable dataTable = new DataTable();
                exec.ExecStoredProcedure("SP_DDL_LISTAR_COLOR_VEHI", dataTable, Parameters);
                foreach (DataRow rows in dataTable.Rows)
                {
                    VehiculoColor_dto entidad = new VehiculoColor_dto();
                    entidad.id_tipo_vehiculo_color = int.Parse(rows["id_tipo_vehiculo_color"].ToString());
                    entidad.descr_tipo_color = rows["descr_tipo_color"].ToString();
                    retorno.Add(entidad);
                    entidad = null;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return retorno;
        }

    }
}
