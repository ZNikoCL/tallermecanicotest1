﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Taller_Datos;

namespace Taller_Negocio
{
    public class Compartido_Negocio
    {
        public  string Encriptar(string _cadena)
        {
            string result = string.Empty;
            byte[] encryted = System.Text.Encoding.Unicode.GetBytes(_cadena);
            result = Convert.ToBase64String(encryted);
            return result;
        }

        public List<Servicio_dto> ListarEstado()
        {
            OracleComand exec = new OracleComand();

            List<Servicio_dto> retorno = new List<Servicio_dto>();
            try
            {
                var Parameters = new Dictionary<string, string>();
                DataTable dataTable = new DataTable();
                exec.ExecStoredProcedure("SP_DDL_LISTAR_ESTADO_RESERVA", dataTable, Parameters);
                foreach (DataRow rows in dataTable.Rows)
                {
                    Servicio_dto entidad = new Servicio_dto();
                    entidad.id_estado_r = rows["id_estado_r"].ToString();
                    entidad.id_descr_estado_r = rows["id_descr_estado_r"].ToString();
                    retorno.Add(entidad);
                    entidad = null;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            return retorno;
        }



    }
}
