﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taller_Datos;
using System.Data;
using Taller_Negocio;

namespace Taller_Negocio
{
    public class Servicio_Negocio
    {
        public bool crearServicio(string id_servicio, string desc_servicio, string valor_servicio)
        {
            OracleComand exec = new OracleComand();
            Compartido_Negocio compartido = new Compartido_Negocio();
            bool respuesta = false;
            var Parameters = new Dictionary<string, string>();
            try
            {
                Parameters.Add("v_desc_servicio", desc_servicio);
                Parameters.Add("v_valor_servicio", valor_servicio.ToString());
                exec.ExecStoredProcedure("SP_CREAR_SERVICIO_CON_ID", Parameters);
                respuesta = true;
            }

            catch (Exception e)
            {
                string mensaje = e.Message.ToString();
                respuesta = false;
            }
            return respuesta;

        }
    }
}
