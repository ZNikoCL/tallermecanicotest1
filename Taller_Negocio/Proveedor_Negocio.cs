﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taller_Datos;
using System.Data;
using Taller_Negocio;


namespace Taller_Negocio
{
    public class Proveedor_Negocio
    {
        /// <summary>
        /// Método que Guarda / Edita empledo
        /// </summary>
        /// <param name="razon_social_prov"></param>
        /// <param name="giro_prov"></param>
        /// <param name="rut_prov"></param>
        /// <param name="dv_prov"></param>
        /// <param name="direccion_prov"></param>
        /// <param name="numeracion_prov"></param>
        /// <param name="fono_prov"></param>
        /// <param name="correo_prov"></param>
        /// <param name="nombre_usu_prov"></param>
        /// <param name="contrasena_prov"></param>
        /// <param name="id_comuna"></param>
        /// <returns></returns>
        public bool crearProveedor(string razon_social_prov, string giro_prov, string rut_prov, string dv_prov,
            string direccion_prov, string numeracion_prov, string fono_prov, string correo_prov, string nombre_usu_prov,
            string contrasena_prov, string id_comuna)
        {
            OracleComand exec = new OracleComand();
            Compartido_Negocio compartido = new Compartido_Negocio();
            bool respuesta = false;
            var Parameters = new Dictionary<string, string>();
            try
            {
                Parameters.Add("v_razon_social_prov", razon_social_prov);
                Parameters.Add("v_giro_prov", giro_prov);
                Parameters.Add("v_rut_prov", rut_prov.ToString());
                Parameters.Add("v_dv_prov", dv_prov.ToString());
                Parameters.Add("v_direccion_prov", direccion_prov);
                Parameters.Add("v_numeracion_prov", numeracion_prov);
                Parameters.Add("v_fono_prov", fono_prov.ToString());
                Parameters.Add("v_correo_prov", correo_prov);
                Parameters.Add("v_nombre_usu_prov", nombre_usu_prov);
                Parameters.Add("v_contrasena_prov", compartido.Encriptar(contrasena_prov));
                Parameters.Add("v_id_comuna", id_comuna.ToString());
                exec.ExecStoredProcedure("SP_CREAR_PROVEEDOR", Parameters);
                respuesta = true;
            }
            catch(Exception ex)
            {
                string mensaje = ex.Message.ToString();
                respuesta = false;
            }
            return respuesta;
        }

        public bool EliminarProveedor(int ID_PROVEEDOR)

        {
            OracleComand exec = new OracleComand();
            bool respuesta = false;
            var Parameters = new Dictionary<string, string>();
            try
            {
                Parameters.Add("v_ID_PROV", ID_PROVEEDOR.ToString());

                exec.ExecStoredProcedure("SP_DELETEPROVEEDOR ", Parameters);
                respuesta = true;
            }
            catch (Exception e)
            {
                string mensaje = e.Message.ToString();
                respuesta = false;
            }
            return respuesta;
        }

        public DataTable ListarProveedor(string RUT_PROVEEDOR)
        {
            OracleComand exec = new OracleComand();
            DataTable dataTable = new DataTable();
            var Parameters = new Dictionary<string, string>();
            try
            {

                Parameters.Add("v_rut_prov;", RUT_PROVEEDOR);
                exec.ExecStoredProcedure("SP_LISTAR_PROVEEDOR", dataTable, Parameters);

                return dataTable;
            }
            catch (Exception e)
            {
                string mensaje = e.Message.ToString();
            }
            return dataTable;
        }
        

    }
}
