﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taller_Datos;
using System.Data;
using Taller_Negocio;

namespace Taller_Negocio
{
    public class EmpresaCli_Negocio
    {
        public bool crearEmpresa(string id_emp, string giro_empresa, string razon_social_Empr, string rut_empresa, string dv_empresa, string direccion_empresa, string numeracion_empresa,
                     string fono_empresa, string id_empresa)
        {
            OracleComand exec = new OracleComand();
            bool respuesta = false;
            Compartido_Negocio compartido = new Compartido_Negocio();
            var Parameters = new Dictionary<string, string>();
            try
            {
                Parameters.Add("V_GIRO_EMPRESA", giro_empresa.ToString());
                Parameters.Add("V_RAZON_SOCIAL_EMPR", razon_social_Empr);
                Parameters.Add("V_RUT_EMPRESA", rut_empresa.ToString());
                Parameters.Add("V_DV_EMPRESA", dv_empresa.ToString());
                Parameters.Add("V_DIRECCION_EMPRESA", direccion_empresa);
                Parameters.Add("V_NUMERACION_EMPRESA", numeracion_empresa);
                Parameters.Add("V_FONO_EMPRESA", fono_empresa.ToString());
                exec.ExecStoredProcedure("SP_CREAR_EMPRESA", Parameters);
                respuesta = true;
            }

            catch (Exception e)
            {
                string mensaje = e.Message.ToString();
                respuesta = false;
            }
            return respuesta;
        }
    }
}
