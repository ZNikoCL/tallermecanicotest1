﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taller_Datos;
using System.Data;

namespace Taller_Negocio
{
    public class Vehiculo_Negocio
    {
        public bool crearVehiculo(string id_vehiculo, string patente, string marca, string modelo, string color, string id_cliente, string id_tipo)
        {
            OracleComand exec = new OracleComand();
            Compartido_Negocio compartido = new Compartido_Negocio();
            bool respuesta = false;
            var Parameters = new Dictionary<string, string>();
            try
            {
                Parameters.Add("v_patente", patente);
                Parameters.Add("v_marca", marca);
                Parameters.Add("v_modelo", modelo);
                Parameters.Add("v_color", color);
                Parameters.Add("v_id_tipo", id_tipo.ToString());
                exec.ExecStoredProcedure("SP_CREAR_VEHICULO3", Parameters);
                respuesta = true;
            }

            catch (Exception e)
            {
                string mensaje = e.Message.ToString();
                respuesta = false;
            }
            return respuesta;
        }
    }
}
