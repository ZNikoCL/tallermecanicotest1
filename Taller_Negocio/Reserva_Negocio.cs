﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Taller_Datos;
using System.Data;

namespace Taller_Negocio
{
    public class Reserva_Negocio
    {


        public DataTable ListarReservaPorIDCliente(string ID_RESERVA)
        {
            OracleComand exec = new OracleComand();
            DataTable dataTable = new DataTable();
            var Parameters = new Dictionary<string, string>();
            try
            {
                Parameters.Add("v_id_reserva", ID_RESERVA);
                exec.ExecStoredProcedure("SP_CREAR_ESTADO_RESERVA", dataTable, Parameters);

                return dataTable;
            }
            catch (Exception e)
            {
                string mensaje = e.Message.ToString();
            }
            return dataTable;
        }

        public DataTable ListarReservaPorRutCliente(string RUT_RESERVA)
        {
            OracleComand exec = new OracleComand();
            DataTable dataTable = new DataTable();
            var Parameters = new Dictionary<string, string>();
            try
            {
                Parameters.Add("v_rut_cliente", RUT_RESERVA);
                exec.ExecStoredProcedure("SP_CREAR_ESTADO_RESERVA2", dataTable, Parameters);

                return dataTable;
            }
            catch (Exception e)
            {
                string mensaje = e.Message.ToString();
            }
            return dataTable;
        }

        public bool actualizarEstadoReserva(string id_reserva, string estado_reserva)
        {
            OracleComand exec = new OracleComand();
            Reserva_Negocio ReservaN = new Reserva_Negocio();
            bool respuesta = false;
            var Parameters = new Dictionary<string, string>();
            try
            {
                Parameters.Add("v_id_reserva", id_reserva.ToString());
                Parameters.Add("v_new_reserva_estado",estado_reserva);
                exec.ExecStoredProcedure("SP_CAMBIAR_ESTADO_RESERVA",Parameters);
                respuesta = true;
            }
            catch(Exception ex)
            {
                string mensaje = ex.Message.ToString();
                respuesta = false;
            }
            return respuesta;
        }
    }
}
